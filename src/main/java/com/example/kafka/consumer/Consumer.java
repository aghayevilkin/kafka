package com.example.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class Consumer {

    @KafkaListener(topics = "testkafka", groupId = "example")
    public void listenGroupFoo(String message) throws InterruptedException {
        log.info("Received Message in group example {} ", message);
        Thread.sleep((long) (Math.random() * 10000));
    }

    @KafkaListener(topics = "testkafka", groupId = "example")
    public void listenGroupFoo2(String message) throws InterruptedException {
        log.info("Received Message in group example {} ", message);
        Thread.sleep((long) (Math.random() * 10000));
    }
    @KafkaListener(topics = "testkafka", groupId = "example1")
    public void listenGroupFoo3(String message) throws InterruptedException {
        log.info("Received Message in group example1 {} ", message);
        Thread.sleep((long) (Math.random() * 10000));
    }
}
